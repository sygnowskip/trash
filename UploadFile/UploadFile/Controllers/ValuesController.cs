﻿using System;
using Microsoft.AspNetCore.Mvc;
using UploadFile.Models;

namespace UploadFile.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // POST api/values
        [HttpPost]
        public ActionResult Post(FileModel request)
        {
            Console.WriteLine(request.Id);
            Console.WriteLine(request.Name);
            Console.WriteLine(request.File.FileName);

            return Ok();
        }
    }
}