﻿using Microsoft.AspNetCore.Http;

namespace UploadFile.Models
{
    public class FileModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public IFormFile File { get; set; }
    }
}